// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


module.exports = mongoose.model('RefreshTokens', new Schema({ 
    public: 		{ type: String, required: true, index: true },
    token: 			{ type: String, required: true }
}));