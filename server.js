// =======================
// get the packages we need ============
// =======================
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
const bcrypt    = require('bcrypt');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file
var User   = require('./app/models/user'); // get our mongoose model
var RefreshTokens   = require('./app/models/refreshToken'); // get our mongoose model
var uuid = require("uuid");
var randtoken = require('rand-token') 


// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
var SALT_WORK_FACTOR = 10;

mongoose.connect(config.database, {
  authMechanism: 'ScramSHA1',
  server: {
    socketOptions: {
      socketTimeoutMS: 0,
      connectionTimeout: 0
    }
  }
});
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

// =======================
// routes ================
// =======================
// basic route
app.get('/', function(req, res) {
    res.send('Hello! The API is at http://localhost:' + port + '/api');
});



// API ROUTES -------------------
// get an instance of the router for api routes
var apiRoutes = express.Router(); 


// route to signup (PASSWORD HASHED) (POST http://localhost:8080/api/signup)
apiRoutes.post('/signup', function(req, res) {
  
  let username = req.body.username;
  let password = req.body.password;
  let hashedPassword = bcrypt.hashSync(password, 10);

  // define model -> app/models/user.js
  // public di generate saat pertama kali mendaftar, ini digunakan untuk melakukan interaksi antara server dan client, public digunakan untuk melakukan invalidasi token
  var user = new User({ 
    auth: true,
    public: uuid.v4(),
    username: username, 
    password: hashedPassword
  });

  user.save(function(err) {
    if (err) throw err;
    console.log('User saved successfully');
    res.json({ success: true });
  });

});

// route to login (PASSWORD HASHED) (POST http://localhost:8080/api/login)
// INI UNTUK LOGIN MENGGUNAKAN HASD-ED PASSWORD
apiRoutes.post('/login', function(req, res) {

  // Check request Body, apakah ada terdapat data yang dibutuhkan
  // REQUIRED : username; password
  if(req.body.constructor === Object && Object.keys(req.body).length > 0 && typeof req.body.username != 'undefined' && typeof req.body.password != 'undefined') {
        // find the user
        User.findOne({
          username: req.body.username
        }, function(err, user) {
          if (err) throw err;
          if (!user) {
            res.status(403).json({ success: false, message: 'Authentication failed. User not found.' })
          } else if (user) {
              // User is found, let's compare sent password vs our stored (with hash) password
              if(bcrypt.compareSync(req.body.password, user.password)) {
              
               res.json(generateLoginJWT(user._id, user.public, req.headers['user-agent'], 'Enjoy your token'));

              } else {
                res.status(403).json({ success: false, message: 'Authentication failed. Password do not match.' })
              }
          }
        });
  }else{
      res.status(400).json({ success: false, message: 'Bad Request' })
  }

});

// return self information (GET http://localhost:8080/api/me)
apiRoutes.post('/me', ensureAuthorized, function(req, res) {

// req.publicToken diambil dari ensureAuthorized();
// Untuk mengambil data, dibutuhkan 3 parameter, yaitu : public. _id dan auth
// public digenerate saat mendaftar, dan hanya akan berubah apabila user melakukan perubahan password

   User.findOne({
    public:req.userToken,
    _id:req.userId,
    auth:true
  }, function(err, results){
      if (err) {                     
        console.log("errr",err); 
      }
      if (results) {
        res.json({
          success: true,
          message: 'Token verified, ini adalah data yang kami ambil berdasarkan token anda.',
          user_data: results
        });
      } else {
          res.status(403).json({ success: false, message: 'Your token has expired, please re-login!,'})
      }
   });
});

// REFRESH TOKEN DISINI (GET http://localhost:8080/api/token?token=REFRESH_TOKEN_DISINI)
apiRoutes.get('/token', function(req, res) {
    
  if(req.query.token){
      var refreshToken = req.query.token;
      jwt.verify(refreshToken, app.get('superSecret'), function(err, decoded) {
          if(err){
              res.status(403).json({ success: false, message: 'Failed to authenticate token.', meta: err })
          }else{

                RefreshTokens.findOneAndRemove({
                  token: decoded.refreshToken
                }, function(tokenErr, tokenResult) {
                    if (tokenErr) throw tokenErr;
                    if(tokenResult){

                          User.findOne({
                            public:tokenResult.public
                          }, function(userErr, userResult){

                              if(userResult){
                               res.json(generateLoginJWT(userResult._id, userResult.public, req.headers['user-agent'], 'Enjoy your new token'));
                              }
                          });
                    }else{
                        res.status(403).json({ success: false, message: 'Refresh token is invalid, please relogin.', meta: err })
                    }
                });

          }
      });


  }else{
    res.status(400).json({ success: false, message: 'Token is required'})
  }

});

// route to show a random message (GET http://localhost:8080/api/)
apiRoutes.get('/', function(req, res) {
  res.json({ message: 'Welcome to the coolest API on earth!' });
});

// route to return all users (GET http://localhost:8080/api/users)
apiRoutes.get('/users', function(req, res) {
  User.find({}, function(err, users) {
    res.json(users);
  });
});   

// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);

// FUNGSI UNTUK MENGECEK TOKEN
function ensureAuthorized(req, res, next) {
    var bearerToken;
    var bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
        var bearer = bearerHeader.split(" ");
        bearerToken = bearer[1];
        jwt.verify(bearerToken, app.get('superSecret'), function(err, decoded) {      
          if (err) {
            //console.log(err.name)
          // Apabila error karena expired, kirim pemberitahuan ke client, untuk mengirim RefreshToken agar dapat diperbahrui tokennya
          if(err.name == 'TokenExpiredError'){
            return res.status(401).json({ success: false, message: 'Token Expired, please reauthenticate with refresh token.', needRefresh: true})
          }else{
            return res.status(403).json({ success: false, message: 'Failed to authenticate token.', meta: err })
          }

          } else {
            if(req.headers['user-agent'] == decoded.agent){
            // if everything is good, save to request for use in other routes
            req.userToken = decoded.public;   
            req.userId = decoded.identifier; 
            next();
            }else{
            return res.status(403).json({ success: false, message: 'Failed to authenticate token. Diffrent User agent detected, please relogin to get a new token'})
            }
          }
        });

        //next();
    } else {
      return res.status(403).send({ 
          success: false, 
          message: 'No token provided.' 
      });
    }
}

// fungsi ini untuk generate token, biar gak berkali kali, cukup panggil aja
// cara panggil : res.json(generateLoginJWT());
function generateLoginJWT(identifier, public, agent, message){
    // Generate refresh token for further use
    var refreshToken = randtoken.uid(256);

    // Simpan Refresh Token beserta public ID ke database
    new RefreshTokens({ 
      public: public,
      token: refreshToken
    }).save();

    // create a token
    var token = jwt.sign({ identifier: identifier, public: public, agent:agent  }, app.get('superSecret'), {
       expiresIn : 3600 // 1 hour
    });

    var successData = {
      success: true,
      message: message,
      token: token,
      refreshToken: jwt.sign({  refreshToken: refreshToken }, app.get('superSecret'))
    };

    return successData;
}

// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);